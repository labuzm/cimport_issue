from distutils.core import setup
from Cython.Build import cythonize

extensions = [
    'restaurant.pyx',
    'B/b.pyx'
]
setup(
    ext_modules=cythonize(extensions)
)
